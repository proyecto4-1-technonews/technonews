/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import modelo.dao.UsuariosDao;
import modelo.entidad.Usuarios;

/** @DARIO MACIAS MADRID */

@ManagedBean
@ViewScoped
public class UsuariosControl {

    /**
     * Creates a new instance of usuariosControl
     */
    private List<Usuarios> listaUsuarios;
    private Usuarios banco;

    public UsuariosControl() {
        banco = new Usuarios();
    }

    public List<Usuarios> getListaUsuarios() {
        UsuariosDao ad = new UsuariosDao();
        listaUsuarios = ad.listarUsuarios();
        return listaUsuarios;
    }

    public void setListaUsuarios(List<Usuarios> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

    public Usuarios getUsuarios() {
        return banco;
    }

    public void setUsuarios(Usuarios banco) {
        this.banco = banco;
    }

    public void limpiarUsuarios() {
        banco = new Usuarios();
    }

    public void agregarUsuarios() {
        UsuariosDao ad = new UsuariosDao();
        ad.agregar(banco);
    }

    public void modificarUsuarios() {
        UsuariosDao ad = new UsuariosDao();
        ad.modificar(banco);
        limpiarUsuarios();
    }

    public void eliminarUsuarios() {
        UsuariosDao ad = new UsuariosDao();
        ad.eliminar(banco);
        limpiarUsuarios();
    }
    
}

