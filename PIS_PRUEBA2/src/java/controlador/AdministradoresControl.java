/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import modelo.dao.AdministradoresDao;
import modelo.entidad.Administradores;


/**
 *
 * @author elcon
 */
@ManagedBean
@ViewScoped
public class AdministradoresControl {

    /**
     * Creates a new instance of administradoresControl
     */
    private List<Administradores> listaAdministradores;
    private Administradores banco;

    public AdministradoresControl() {
        banco = new Administradores();
    }

    public List<Administradores> getListaAdministradores() {
        AdministradoresDao ad = new AdministradoresDao();
        listaAdministradores = ad.listarAdministradores();
        return listaAdministradores;
    }

    public void setListaAdministradores(List<Administradores> listaAdministradores) {
        this.listaAdministradores = listaAdministradores;
    }

    public Administradores getAdministradores() {
        return banco;
    }

    public void setAdministradores(Administradores banco) {
        this.banco = banco;
    }

    public void limpiarAdministradores() {
        banco = new Administradores();
    }

    public void agregarAdministradores() {
        AdministradoresDao ad = new AdministradoresDao();
        ad.agregar(banco);
    }

    public void modificarAdministradores() {
        AdministradoresDao ad = new AdministradoresDao();
        ad.modificar(banco);
        limpiarAdministradores();
    }

    public void eliminarAdministradores() {
        AdministradoresDao ad = new AdministradoresDao();
        ad.eliminar(banco);
        limpiarAdministradores();
    }
}

