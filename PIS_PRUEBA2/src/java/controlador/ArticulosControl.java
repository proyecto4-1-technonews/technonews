/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import modelo.dao.ArticulosDao;
import modelo.entidad.Articulos;


/**
 *
 * @author elcon
 */
@ManagedBean
@ViewScoped
public class ArticulosControl {

    /**
     * Creates a new instance of ArticulosControl
     */
    private List<Articulos> listaArticulos;
    private Articulos banco;

    public ArticulosControl() {
        banco = new Articulos();
    }

    public List<Articulos> getListaArticulos() {
        ArticulosDao ad = new ArticulosDao();
        listaArticulos = ad.listarArticulos();
        return listaArticulos;
    }

    public void setListaArticulos(List<Articulos> listaArticulos) {
        this.listaArticulos = listaArticulos;
    }

    public Articulos getArticulos() {
        return banco;
    }

    public void setArticulos(Articulos banco) {
        this.banco = banco;
    }

    public void limpiarArticulos() {
        banco = new Articulos();
    }

    public void agregarArticulos() {
        ArticulosDao ad = new ArticulosDao();
        ad.agregar(banco);
    }

    public void modificarArticulos() {
        ArticulosDao ad = new ArticulosDao();
        ad.modificar(banco);
        limpiarArticulos();
    }

    public void eliminarArticulos() {
        ArticulosDao ad = new ArticulosDao();
        ad.eliminar(banco);
        limpiarArticulos();
    }
}

