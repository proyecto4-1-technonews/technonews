package modelo.entidad;
// Generated 04/10/2020 16:26:08 by Hibernate Tools 4.3.1


import java.math.BigDecimal;

/**
 * Administradores generated by hbm2java
 */
public class Administradores  implements java.io.Serializable {


     private BigDecimal idAdmin;
     private String cedulaAdmin;
     private String nombres;
     private String apellidos;
     private BigDecimal edad;
     private String telefono;
     private String email;
     private String sexo;
     private String contrasena;
     private String aceptaCondiciones;

    public Administradores() {
    }

	
    public Administradores(BigDecimal idAdmin) {
        this.idAdmin = idAdmin;
    }
    public Administradores(BigDecimal idAdmin, String cedulaAdmin, String nombres, String apellidos, BigDecimal edad, String telefono, String email, String sexo, String contrasena, String aceptaCondiciones) {
       this.idAdmin = idAdmin;
       this.cedulaAdmin = cedulaAdmin;
       this.nombres = nombres;
       this.apellidos = apellidos;
       this.edad = edad;
       this.telefono = telefono;
       this.email = email;
       this.sexo = sexo;
       this.contrasena = contrasena;
       this.aceptaCondiciones = aceptaCondiciones;
    }
   
    public BigDecimal getIdAdmin() {
        return this.idAdmin;
    }
    
    public void setIdAdmin(BigDecimal idAdmin) {
        this.idAdmin = idAdmin;
    }
    public String getCedulaAdmin() {
        return this.cedulaAdmin;
    }
    
    public void setCedulaAdmin(String cedulaAdmin) {
        this.cedulaAdmin = cedulaAdmin;
    }
    public String getNombres() {
        return this.nombres;
    }
    
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }
    public String getApellidos() {
        return this.apellidos;
    }
    
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }
    public BigDecimal getEdad() {
        return this.edad;
    }
    
    public void setEdad(BigDecimal edad) {
        this.edad = edad;
    }
    public String getTelefono() {
        return this.telefono;
    }
    
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    public String getSexo() {
        return this.sexo;
    }
    
    public void setSexo(String sexo) {
        this.sexo = sexo;
    }
    public String getContrasena() {
        return this.contrasena;
    }
    
    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }
    public String getAceptaCondiciones() {
        return this.aceptaCondiciones;
    }
    
    public void setAceptaCondiciones(String aceptaCondiciones) {
        this.aceptaCondiciones = aceptaCondiciones;
    }




}


